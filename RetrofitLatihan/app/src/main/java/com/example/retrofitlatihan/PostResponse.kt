package com.example.retrofitlatihan

import com.google.gson.annotations.SerializedName

data class PostResponse{
    val id: int,
    val title: String?,
    @SerializedName( value: "body")
    val text: String?
}
